/* This file tracks the version of the heyu program
 * It should be manually changed when the version number is ready to be
 * bumped up.
 */
#define VERSION "2.10-rc2"
#define RELEASE_DATE "20110807"
