

/* Formats for displayed values of RFXSensor variables */
#define FMT_RFXT      "%.3f"  /* Temperature */
#define FMT_RFXRH     "%.2f"  /* Relative Humidity */
#define FMT_RFXBP     "%.4g"  /* Barometric Pressure */
#define FMT_RFXVAD    "%.2f"  /* A/D Voltage */

/* Formats for displayed values of RFXMeter variables */
#define FMT_RFXPOWER  "%.3f"  /* Power meter */
#define FMT_RFXWATER  "%.0f"  /* Water meter */
#define FMT_RFXGAS    "%.2f"  /* Gas meter */
#define FMT_RFXPULSE  "%.0f"  /* General pulse meter */
